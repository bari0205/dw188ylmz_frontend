import React, { Component, useState, useContext } from "react";
import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Form, Dropdown, Table } from "react-bootstrap";
import DropdownButton from "react-bootstrap/DropdownButton";
import {
  BrowserRouter as Router,
  Switch,
  Link,
  Route,
  Redirect,
  useHistory,
} from "react-router-dom";
import { CartContext } from "../context/cartContext";

import { useQuery, useMutation } from "react-query";
import { API } from "../config/Api";

import { FaBookMedical, FaCheckCircle, FaSignOutAlt } from "react-icons/fa";

export default function Home() {
  const [modalAdd, setModalAdd] = useState(false);

  const [modalAprove, setModalAprove] = useState(false);
  const [modalCancel, setModalCancel] = useState(false);
  const [bookId, setBookId] = useState(null);

  const [modalSuccess, setModalSuccess] = useState(false);

  const [state, dispatch] = useContext(CartContext);
  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <a
      href=""
      style={{ color: "#007bff00" }}
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
      &#x25bc;
    </a>
  ));

  const [formData, setFormData] = useState({
    userId: `${state.user.id}`,
    categoryId: "",
    title: "",
    author: "",
    publication: "",
    page: "",
    ISBN: "",
    aboutBook: "",
    file: "",
    status: "Aproved",
    image: "",
  });

  const {
    userId,
    categoryId,
    title,
    author,
    publication,
    page,
    ISBN,
    aboutBook,
    file,
    status,
    image,
  } = formData;

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const { data: categoryData } = useQuery("getCategory", () =>
    API.get("/category")
  );

  const { isLoading, error, data: bookData, refetch } = useQuery(
    "getBook",
    () => API.get("/books")
  );

  const [addBook] = useMutation(async () => {
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      const body = JSON.stringify({
        title,
        userId,
        categoryId,
        author,
        publication,
        page,
        ISBN,
        aboutBook,
        file,
        status,
        image,
      });
      const res = await API.post("/books", body, config);

      setFormData({
        userId: `${state.user.id}`,
        categoryId: "",
        title: "",
        author: "",
        publication: "",
        page: "",
        ISBN: "",
        aboutBook: "",
        file: "",
        status: "Aproved",
        image: "",
      });

      setModalAdd(false);
      setModalSuccess(true);
      refetch();
      return res;
    } catch (err) {
      refetch();
      console.log(err);
    }
  });

  const [Aprove] = useMutation(async () => {
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      const body = JSON.stringify({
        status,
      });

      setModalAprove(false);
      const res = await API.patch(`/books/${bookId}`, body, config);
      refetch();
      return res;
    } catch (err) {
      refetch();
      console.log(err);
    }
  });

  const [Cancel] = useMutation(async () => {
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      const body = JSON.stringify({
        status: "Canceled",
      });

      setModalCancel(false);
      const res = await API.patch(`/books/${bookId}`, body, config);

      refetch();
      return res;
    } catch (err) {
      refetch();
      console.log(err);
    }
  });

  return isLoading || !categoryData || !bookData ? (
    <h1>Loading...</h1>
  ) : error ? (
    <h1>Your Error : {error.message}</h1>
  ) : (
    <div>
      <div className="card-header">
        <div className="container">
          <div className="row">
            <div className="col col-md-6">
              <div>
                <img
                  src={require("../images/Icon.png")}
                  style={{ margin: "1% 5%", width: "25%" }}
                ></img>
              </div>
            </div>

            <div className="col col-md-6">
              <div style={{ textAlign: "right", marginRight: "10%" }}>
                <Dropdown>
                  <Dropdown.Toggle
                    as={CustomToggle}
                    id="dropdown-custom-components"
                    style={{ float: "right" }}
                  >
                    <img
                      className="shadowelips"
                      src={require("../images/Ellipse1.png")}
                      style={{ width: "15%" }}
                    ></img>
                  </Dropdown.Toggle>

                  <Dropdown.Menu>
                    <Dropdown.Item onClick={() => setModalAdd(true)}>
                      {" "}
                      <p className="juduldop" style={{ display: "unset" }}>
                        <FaBookMedical /> Add Book
                      </p>
                    </Dropdown.Item>
                    <div>
                      <div>
                        <hr
                          style={{
                            height: "0.5px",
                            backgroundColor: "#C9C9C9",
                            marginTop: "10%",
                          }}
                        />
                      </div>
                    </div>

                    <Dropdown.Item
                      onClick={() =>
                        dispatch({
                          type: "LOGOUT",
                        })
                      }
                    >
                      <p className="juduldop" style={{ display: "unset" }}>
                        <FaSignOutAlt color="red" /> Sign Out
                      </p>
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container">
        <p className="verifikasi">Book Verification</p>
        <div className="row" style={{ marginTop: "3%" }}>
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th>No</th>
                <th>User or Author</th>
                <th>Title</th>
                <th>ISBN</th>
                <th>E-book</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {bookData.data.data.all.map((book, index) => (
                <tr>
                  <td className="isitable">{index + 1}</td>
                  <td className="isitable">{book.author}</td>
                  <td className="isitable">{book.title}</td>
                  <td className="isitable">{book.ISBN}</td>
                  <td className="isitable">{book.file}</td>
                  <td className="isitable">
                    {book.status == "Aproved" ? (
                      <p style={{ color: "#0ACF83" }}> {book.status} </p>
                    ) : book.status == "Waiting" ? (
                      <p style={{ color: "#F7941E" }}> {book.status} </p>
                    ) : (
                      <p style={{ color: "#FF0742" }}> {book.status} </p>
                    )}
                  </td>
                  <td>
                    {book.status == "Aproved" ? (
                      <div
                        style={{
                          color: "#0ACF83",
                          fontSize: "30px",
                          textAlign: "center",
                        }}
                      >
                        <FaCheckCircle />
                      </div>
                    ) : (
                      <div style={{ textAlign: "center" }}>
                        <Button
                          style={{
                            width: "40%",
                            backgroundColor: "#FF0742",
                            borderColor: "#FF0742",
                          }}
                          onClick={() => {
                            setBookId(book.id);
                            setModalCancel(true);
                          }}
                        >
                          Cancel
                        </Button>
                        <div
                          style={{ display: "unset", margin: "0% 2%" }}
                        ></div>
                        <Button
                          style={{
                            width: "40%",
                            backgroundColor: "#0ACF83",
                            borderColor: "#0ACF83",
                          }}
                          onClick={() => {
                            setBookId(book.id);
                            setModalAprove(true);
                          }}
                        >
                          Approve
                        </Button>
                      </div>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>

      <Modal
        size="lg"
        show={modalAdd}
        onHide={() => setModalAdd(false)}
        dialogClassName="modal-90w besardialog"
        contentClassName="besardialog"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton style={{ borderBottom: "white" }}>
          {/* <img src={require('../images/Icon.png')} style={{marginLeft:"0%",marginTop:"2%", width:"13%"}}></img> */}

          {/* <Modal.Title id="example-custom-modal-styling-title">
                    Custom Modal Styling
                </Modal.Title> */}
        </Modal.Header>
        <Modal.Body>
          <div style={{ marginLeft: "5%" }}>
            <div>
              <p className="addbook" style={{ marginTop: "0%" }}>
                Add Book
              </p>
            </div>

            <div style={{ width: "95%", marginTop: "5%" }}>
              <Form
                onSubmit={(e) => {
                  e.preventDefault();
                  addBook();
                  refetch();
                }}
              >
                <Form.Group controlId="">
                  <Form.Control
                    type="text"
                    placeholder="Title"
                    name="title"
                    value={title}
                    onChange={(e) => handleChange(e)}
                  />
                </Form.Group>
                <Form.Group controlId="">
                  <Form.Control
                    type="text"
                    placeholder="Publication Date"
                    name="publication"
                    value={publication}
                    onChange={(e) => handleChange(e)}
                  />
                </Form.Group>
                <Form.Group controlId="">
                  <Form.Control
                    type="text"
                    placeholder="Author"
                    name="author"
                    value={author}
                    onChange={(e) => handleChange(e)}
                  />
                </Form.Group>

                <Form.Group controlId="formGroupPassword">
                  <Form.Control
                    as="select"
                    name="categoryId"
                    value={categoryId}
                    onChange={(e) => handleChange(e)}
                    required
                  >
                    <option value="">Category</option>
                    {categoryData.data.data.all.map((category) => (
                      <option value={category.id}>{category.name}</option>
                    ))}
                  </Form.Control>
                </Form.Group>

                <Form.Group controlId="">
                  <Form.Control
                    type="text"
                    placeholder="Pages"
                    name="page"
                    value={page}
                    onChange={(e) => handleChange(e)}
                  />
                </Form.Group>
                <Form.Group controlId="">
                  <Form.Control
                    type="text"
                    placeholder="ISBN"
                    name="ISBN"
                    value={ISBN}
                    onChange={(e) => handleChange(e)}
                  />
                </Form.Group>

                <Form.Group controlId="">
                  <Form.Control
                    as="textarea"
                    name="aboutBook"
                    placeholder="About Book"
                    style={{ height: "150px" }}
                    value={aboutBook}
                    onChange={(e) => handleChange(e)}
                  />
                </Form.Group>

                <Form.Group controlId="">
                  <Form.Control
                    type="text"
                    placeholder="File Book"
                    name="file"
                    value={file}
                    onChange={(e) => handleChange(e)}
                  />
                </Form.Group>

                <Form.Group controlId="">
                  <Form.Control
                    type="text"
                    placeholder="Image Book"
                    name="image"
                    value={image}
                    onChange={(e) => handleChange(e)}
                  />
                </Form.Group>

                <Form.Group controlId="">
                  <Form.Control
                    type="text"
                    placeholder="Image Book"
                    name="userId"
                    value={userId}
                    onChange={(e) => handleChange(e)}
                    hidden
                  />
                </Form.Group>

                <Form.Group controlId="">
                  <Form.Control
                    type="text"
                    placeholder="Image Book"
                    name="status"
                    value={status}
                    onChange={(e) => handleChange(e)}
                    hidden
                  />
                </Form.Group>

                <div style={{ float: "right" }}>
                  <Button
                    type="submit"
                    variant=""
                    style={{ backgroundColor: "#EE4622" }}
                  >
                    <font className="namabutton">
                      Add Book <FaBookMedical />
                    </font>
                  </Button>
                </div>
              </Form>
            </div>
          </div>
        </Modal.Body>
      </Modal>

      {/* Modal success */}
      <Modal
        size="lg"
        show={modalSuccess}
        onHide={() => setModalSuccess(false)}
        dialogClassName="modal-90w posisimodal"
        contentClassName="besardialog"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Body>
          <div className="ketmod">
            <p style={{ width: "100%" }}>Successfuly Add Book</p>
          </div>
        </Modal.Body>
      </Modal>

      {/* Modal Aprove */}
      <Modal
        size="lg"
        show={modalAprove}
        // onHide={() => setModalAprove(false)}
        dialogClassName="modal-90w posisimodal"
        contentClassName="besardialog"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Body>
          <div className="ketmod">
            <p
              style={{
                width: "100%",
                marginTop: "4%",
                fontSize: "125%",
                color: "teal",
              }}
            >
              Aprove This Book?
            </p>
          </div>
          <div style={{ float: "right" }}>
            <Button
              type="submit"
              variant="info"
              onClick={(e) => {
                setModalAprove(false);
                e.preventDefault();
                Aprove();
              }}
            >
              <font className="namabutton">Submit</font>
            </Button>
          </div>
        </Modal.Body>
      </Modal>

      {/* Modal Cancel */}
      <Modal
        size="lg"
        show={modalCancel}
        // onHide={() => setModalAprove(false)}
        dialogClassName="modal-90w posisimodal"
        contentClassName="besardialog"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Body>
          <div className="ketmod">
            <p
              style={{
                width: "100%",
                marginTop: "4%",
                fontSize: "125%",
                color: "teal",
              }}
            >
              Cancel This Book?
            </p>
          </div>
          <div style={{ float: "right" }}>
            <Button
              type="submit"
              variant="danger"
              onClick={(e) => {
                setModalCancel(false);
                e.preventDefault();
                Cancel();
              }}
            >
              <font className="namabutton">Submit</font>
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}
