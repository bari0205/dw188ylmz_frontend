import React, { useState, useContext } from "react";
import { useQuery, useMutation } from "react-query";
import { API } from "../config/Api";
import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { CartContext } from "../context/cartContext";
import { Button, Dropdown } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";

import { FaChevronLeft } from "react-icons/fa";
import DropdownItem from "react-bootstrap/esm/DropdownItem";

export default function Mainpage() {
  const [state, dispatch] = useContext(CartContext);
  const history = useHistory();

  const [categoryId, setCategoryId] = useState("");

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <a
      href=""
      style={{ color: "#007bff00" }}
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
      &#x25bc;
    </a>
  ));

  //menggunakan react-query
  //getBook
  const { isLoading, error, data: bookData, refetch } = useQuery(
    "getBooks",
    () => API.get(`/book-aprove/${categoryId}`)
  );

  //getCategory
  const { data: categoryData } = useQuery("getCategory", () =>
    API.get("/category")
  );

  const { data: categoryDataChoose } = useQuery("getCategoryChoose", () =>
    API.get(`/category/${categoryId}`)
  );
  //

  //reload
  const [reLoad] = useMutation(async () => {
    refetch();
  });

  return isLoading || !bookData || !categoryData || !categoryDataChoose ? (
    <h1>Loading...</h1>
  ) : error ? (
    <h1>Your Error : {error.message}</h1>
  ) : (
    <div>
      <div
        className="card-header"
        style={{
          height: "60%",
          width: "98%",
          backgroundColor: "#E6F2FD",
          marginTop: "3%",
          borderRadius: "10px",
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col col-md-8">
              <div style={{ marginLeft: "8%", marginTop: "10%" }}>
                <p className="namabannerbesar">
                  Share, read, and <i>love</i>
                </p>
                <p style={{ marginTop: "5%" }} className="namabannerkecil">
                  Reading is Facinating
                </p>
              </div>
            </div>
            <div className="col col-md-4">
              <div>
                <img
                  className="gamabrbukubanner"
                  src={require("../images/bookfix.png")}
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row" style={{ width: "102.5%" }}>
          <div className="col col-md-6">
            <p className="listBook">List Buku</p>
          </div>
          <div
            className="col col-md-6"
            style={{ textAlign: "right", paddingRight: "0" }}
          >
            <Dropdown>
              <Dropdown.Toggle
                as={CustomToggle}
                id="dropdown-custom-components"
              >
                <Button
                  style={{
                    backgroundColor: "rgba(233, 233, 233, 0.7)",
                    borderColor: "rgba(233, 233, 233, 0.7)",
                    color: "black",
                    marginTop: "3%",
                  }}
                >
                  <div className="buttoncategory">
                    <FaChevronLeft />
                    <p style={{ display: "unset" }}>Category</p>
                  </div>
                </Button>
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <DropdownItem
                  onClick={() => {
                    setCategoryId("");
                    reLoad();
                  }}
                >
                  <p>All Category</p>
                </DropdownItem>
                {categoryData.data.data.all.map((category) => (
                  <Dropdown.Item
                    onClick={() => {
                      setCategoryId(category.id);
                      reLoad();
                    }}
                  >
                    <p>{category.name}</p>
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </div>
        <div className="row">
          {bookData.data.data.all.map((book) => (
            <div className="col col-md-3">
              <Link
                style={{ textDecoration: "none" }}
                onClick={() => history.push(`/detail/${book.id}`)}
              >
                <div>
                  <img
                    className="gambarbuku"
                    src={require(`../images/${book.image}`)}
                  />
                  <p className="judulbuku">{book.title}</p>
                  <p className="pengarang">{book.author}</p>
                </div>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
