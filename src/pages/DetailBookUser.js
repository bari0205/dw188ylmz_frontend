import React, { useState, useContext } from "react";
import { Button, Modal, Form } from "react-bootstrap";
import { useParams, useHistory } from "react-router-dom";
import { book } from "../data/Book";
import { useQuery, useMutation } from "react-query";
import { API } from "../config/Api";

import { FaChevronLeft, FaChevronRight } from "react-icons/fa";
import { FaRegBookmark } from "react-icons/fa";
import { CartContext } from "../context/cartContext";

export default function DetailBookUser() {
  const [modalAdd, setModalAdd] = useState(false);
  const [Mark, setMark] = useState(false);
  const history = useHistory();
  const [state, dispatch] = useContext(CartContext);
  const [modalDelete, setModalDelete] = useState(false);

  const [bookId, setBookId] = useState(null);
  const [userId, setUserId] = useState(null);
  const [markId, setMarkId] = useState(null);

  const { id } = useParams();
  const { isLoading, error, data: detailBook, refetch } = useQuery(
    "getDetail",
    () => API.get(`/books/${id}`)
  );

  function checkMark() {
    const mark = detailBook.data.data.detail.Libraries.some(
      (mark) =>
        detailBook.data.data.detail.id === mark.bookId &&
        state.user.id === mark.userId
    );
    console.log(mark);
    return mark;
  }

  const [storeMark] = useMutation(async () => {
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      const body = JSON.stringify({ bookId: `${bookId}`, userId: `${userId}` });
      const res = await API.post("/mark", body, config);
      refetch();
      return res;
    } catch (err) {
      refetch();
      console.log(err);
    }
  });

  const [deleteMark] = useMutation(async () => {
    try {
      const res = await API.delete(`/mark/${markId}`);
      refetch();
    } catch (err) {
      refetch();
      console.log(err);
    }
  });

  return isLoading || !detailBook ? (
    <h1>Loading...</h1>
  ) : error ? (
    <h1>Your Error : {error.message}</h1>
  ) : (
    <div>
      <div className="Container">
        <div className="row">
          <div className="col col-md-5" style={{ paddingRight: "0" }}>
            <img
              className="gambardetail"
              src={require(`../images/${detailBook.data.data.detail.image}`)}
            />
          </div>

          <div className="col col-md-7">
            <div className="detailnya">
              <div>
                <p className="juduldetail">
                  {detailBook.data.data.detail.title}
                </p>
                <p className="subjuduldetail">
                  {detailBook.data.data.detail.author}
                </p>
              </div>

              <div style={{ marginTop: "8%" }}>
                <p className="isidetail">Publication</p>
                <p className="subisidetail">
                  {detailBook.data.data.detail.publication}
                </p>
              </div>

              <div style={{ marginTop: "5%" }}>
                <p className="isidetail">Category</p>
                <p className="subisidetail">
                  {detailBook.data.data.detail.bookCategory.name}
                </p>
              </div>

              <div style={{ marginTop: "5%" }}>
                <p className="isidetail">Pages</p>
                <p className="subisidetail">
                  {detailBook.data.data.detail.page}
                </p>
              </div>

              <div style={{ marginTop: "5%" }}>
                <p className="isidetail" style={{ color: "#EE4622" }}>
                  ISBN
                </p>
                <p className="subisidetail">
                  {detailBook.data.data.detail.ISBN}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div style={{ marginLeft: "0%", width: "80%" }}>
        <hr style={{ height: "0.5px", backgroundColor: "#C9C9C9" }} />
      </div>
      <div className="container">
        <div className="row">
          <div style={{ marginTop: "3%" }}>
            <p className="aboutdetail">About This Book</p>
          </div>

          <div style={{ width: "81%" }}>
            <p className="subaboutdetail">
              {detailBook.data.data.detail.aboutBook}
            </p>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row" style={{ float: "right", width: "34.5%" }}>
          <div>
            <button
              className="btn"
              style={{
                color: "black",
                backgroundColor: "rgba(233, 233, 233, 0.7)",
                marginLeft: "10%",
                width: "100%",
              }}
              onClick={() =>
                history.push(`/pdf/${detailBook.data.data.detail.id}`)
              }
            >
              Read Book <FaChevronRight />
            </button>
          </div>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
        </div>
      </div>

      <Modal
        size="lg"
        show={modalAdd}
        onHide={() => setModalAdd(false)}
        dialogClassName="modal-90w posisimodal"
        contentClassName="besardialog"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Body>
          <div className="ketmod">
            <p style={{ width: "100%" }}>
              Your book has been added successfully
            </p>
          </div>
        </Modal.Body>
      </Modal>

      {/* Modal Delete */}
      <Modal
        size="lg"
        show={modalDelete}
        onHide={() => setModalDelete(false)}
        dialogClassName="modal-90w posisimodal"
        contentClassName="besardialog"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Body>
          <div className="ketmod">
            <p style={{ width: "100%", color: "orangered" }}>
              Your book has been successfully delete from library
            </p>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}
