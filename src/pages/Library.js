import React, { useState, useContext } from "react";
import { CartContext } from "../context/cartContext";
import { Link, useHistory } from "react-router-dom";
import { useQuery } from "react-query";
import { API } from "../config/Api";
export default function Library() {
  const history = useHistory();
  const [state, dispatch] = useContext(CartContext);

  const { isLoading, error, data: bookMark, refetch } = useQuery(
    "getMark",
    () => API.get(`/mark/${state.user.id}`)
  );
  return isLoading || !bookMark ? (
    <h1>Loading..</h1>
  ) : (
    <div>
      <div className="container">
        <p className="listBook">My Library</p>
        <div className="row">
          {bookMark.data.data.detail.map((mark) => (
            <div className="col col-md-3">
              <Link
                style={{ textDecoration: "none" }}
                onClick={() => history.push(`/detail/${mark.bookId}`)}
              >
                <div>
                  <img
                    className="gambarbuku"
                    src={require(`../images/${mark.libraryBook.image}`)}
                  />
                  <p className="judulbuku">{mark.libraryBook.title}</p>
                  <p className="pengarang">{mark.libraryBook.author}</p>
                </div>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
