import React, { Component } from "react";
import { ReactReader } from "react-reader";
import { useParams, useHistory } from "react-router-dom";
import { book } from "../data/Book";

import { useQuery, useMutation } from "react-query";
import { API } from "../config/Api";

export default function Readerpdf() {
  const { id } = useParams();
  const { isLoading, error, data: detailBook, refetch } = useQuery(
    "getDetail",
    () => API.get(`/books/${id}`)
  );

  return isLoading || !detailBook ? (
    <h1>Loading...</h1>
  ) : error ? (
    <h1>Your Error : {error.message}</h1>
  ) : (
    <div style={{ position: "relative", height: "100vh" }}>
      <ReactReader
        url={require(`../pdf/${detailBook.data.data.detail.file}`)}
        title={detailBook.data.data.detail.file}
        location={"epubcfi(/6/2[cover]!/6)"}
        locationChanged={(epubcifi) => console.log(epubcifi)}
      />
    </div>
  );
}
