import React, { useState, useContext } from "react";
import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Form } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Switch,
  Link,
  Route,
  Redirect,
  useHistory,
  useRouteMatch,
} from "react-router-dom";
import { CartContext } from "../context/cartContext";

import { API, setAuthToken } from "../config/Api";
import { useQuery, useMutation } from "react-query";

import { FaRegUser, FaUserAlt } from "react-icons/fa";
import { FaBook } from "react-icons/fa";
import { FaBookMedical } from "react-icons/fa";
import { FaSignOutAlt } from "react-icons/fa";

import { BiBookAdd, BiExit } from "react-icons/bi";
import { TiThLis } from "react-icons/ti";
import { VscLibrary } from "react-icons/vsc";

export default function Navbar() {
  const [state, dispatch] = useContext(CartContext);
  const history = useHistory();

  const {
    isLoading,
    error,
    data: profileData,
    refetch,
  } = useQuery("getProfile", () => API.get(`/user/${state.user.id}`));

  return isLoading || !profileData ? (
    <h1>Loading...</h1>
  ) : (
    <div>
      <div>
        <Link to="/main">
          <img
            src={require("../images/Icon.png")}
            style={{ marginLeft: "-10%", marginTop: "9%", width: "60%" }}
          ></img>
        </Link>
      </div>
      <div>
        <img
          className="shadowelips"
          src={profileData.data.data.detail.image}
          style={{ marginTop: "10%", width: "30%" }}
        ></img>
      </div>
      <div className="eginame">
        <font>{state.user?.fullName}</font>
      </div>
      <div style={{ marginLeft: "20%" }}>
        <hr
          style={{
            height: "0.5px",
            backgroundColor: "#C9C9C9",
            marginTop: "10%",
          }}
        />
      </div>
      <div style={{ marginLeft: "20%", textAlign: "left", marginTop: "10%" }}>
        <Link
          className="linkmenu"
          style={{ color: "black", textDecoration: "none" }}
          to="/profile"
          activeClassName="active"
        >
          <div className="buttonmenu">
            <div style={{ marginLeft: "1%", padding: "8%" }}>
              <FaRegUser size={24} />{" "}
              <font className="namamenu"> Profile </font>
            </div>
          </div>
        </Link>

        <Link
          className="linkmenu"
          style={{ color: "black", textDecoration: "none" }}
          to="/library"
        >
          <div className="buttonmenu">
            <div style={{ marginLeft: "1%", padding: "8%" }}>
              <VscLibrary size={24} />{" "}
              <font className="namamenu"> My Library </font>
            </div>
          </div>
        </Link>

        <Link
          className="linkmenu"
          style={{ color: "black", textDecoration: "none" }}
          to="/add"
        >
          <div className="buttonmenu">
            <div style={{ marginLeft: "1%", padding: "8%" }}>
              <BiBookAdd size={24} />{" "}
              <font className="namamenu"> Add Book </font>
            </div>
          </div>
        </Link>
      </div>

      <div style={{ marginTop: "10%", marginLeft: "20%" }}>
        <hr
          style={{
            height: "0.5px",
            backgroundColor: "#C9C9C9",
            marginTop: "10%",
          }}
        />
      </div>
      <div style={{ marginLeft: "20%", textAlign: "left" }}>
        <Link className="linkmenu" style={{ color: "black" }}>
          <button
            className="buttonmenu"
            onClick={() =>
              dispatch({
                type: "LOGOUT",
              })
            }
          >
            <div style={{ marginLeft: "5%" }}>
              <BiExit size={24} /> <font className="namamenu"> Logout </font>
            </div>
          </button>
        </Link>
      </div>
    </div>
  );
}
